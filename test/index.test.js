import * as test from '../src';

describe('Creating and returning promises', () => {
  it('getResult resolves with the string "complete"', done => {
    test
      .getResult()
      .then(val => {
        expect(val).to.equal('complete');
        done();
      })
      .catch(done);
  });

  it('getResultFails rejects with an error containing the message "failed', done => {
    test
      .getResultFails()
      .then(() => {
        fail('did not reject');
      })
      .catch(err => {
        expect(err).to.be.an.error();
        expect(err.message).to.equal('failed');
        done();
      });
  });
});

describe('Chaining promises', () => {
  it('chaining promises returns result after last promise resolves', done => {
    test
      .getResultsChained(12, 8)
      .then(result => {
        expect(result).to.equal(40);
        done();
      })
      .catch(done);
  });

  it('chaining promises rejects with the message "failed: <resultOfFirstPromise>" after last promise rejects', done => {
    test
      .getResultsChainedFails(12, 8)
      .then(() => {
        fail('did not reject');
        done();
      })
      .catch((err) => {
        expect(err).to.be.an.error();
        expect(err.message).to.equal('failed 20');
        done();
      });
  });
});

describe('Promise methods', () => {

  it('getAllResults returns the results from running results 1-3 from the worker module', (done) => {
    test.getAllResults().then(results => {
      expect(results).to.be.an.array();
      expect(results).to.equal([5, 10, 15]);
      done()
    })
    .catch(done);
  });

  it('getFirstResult returns the first result from running results 4-6 from the worker module', (done) => {
    test.getFirstResult().then(result => {
      expect(result).to.equal(25);
      done();
    })
    .catch(done);
  });

});

describe('async/await', () => {
  it('getAsyncResult is defined and resolves with the awaited result of calling result7 from the worker module', (done) => {
    expect(test.getAsyncResult).to.be.a.function();

    test.getAsyncResult('Helio').then(result => {
      expect(result).to.equal('helio');
      done();
    })
    .catch(done);
  })
})

